import 'dart:convert';

List<StoreShopData> storeShopDataFromJson(String str) =>
    List<StoreShopData>.from(
        json.decode(str).map((x) => StoreShopData.fromJson(x)));

String storeShopDataToJson(List<StoreShopData> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class StoreShopData {
  StoreShopData({
    this.sid,
    this.cost,
    this.name,
    this.phoneStore,
    this.latitudeS,
    this.longtitudeS,
    this.descriptionStore,
    this.addressStore,
    this.picStore,
    this.tid,
    this.numberAccount,
    this.nameBank,
    this.promptPay,
  });

  String? sid;
  String? cost;
  String? name;
  String? phoneStore;
  String? latitudeS;
  String? longtitudeS;
  String? descriptionStore;
  String? addressStore;
  String? picStore;
  String? tid;
  String? numberAccount;
  String? nameBank;
  String? promptPay;

  factory StoreShopData.fromJson(Map<String, dynamic> json) => StoreShopData(
        sid: json["SID"] == null ? null : json["SID"],
        cost: json["Cost"] == null ? null : json["Cost"],
        name: json["Name"] == null ? null : json["Name"],
        phoneStore: json["Phone_Store"] == null ? null : json["Phone_Store"],
        latitudeS: json["Latitude_S"] == null ? null : json["Latitude_S"],
        longtitudeS: json["Longtitude_S"] == null ? null : json["Longtitude_S"],
        descriptionStore: json["Description_Store"] == null
            ? null
            : json["Description_Store"],
        addressStore:
            json["Address_Store"] == null ? null : json["Address_Store"],
        picStore: json["Pic_Store"] == null ? null : json["Pic_Store"],
        tid: json["TID"] == null ? null : json["TID"],
        numberAccount:
            json["Number_Account"] == null ? null : json["Number_Account"],
        nameBank: json["Name_Bank"] == null ? null : json["Name_Bank"],
        promptPay: json["Prompt_pay"] == null ? null : json["Prompt_pay"],
      );

  Map<String, dynamic> toJson() => {
        "SID": sid == null ? null : sid,
        "Cost": cost == null ? null : cost,
        "Name": name == null ? null : name,
        "Phone_Store": phoneStore == null ? null : phoneStore,
        "Latitude_S": latitudeS == null ? null : latitudeS,
        "Longtitude_S": longtitudeS == null ? null : longtitudeS,
        "Description_Store": descriptionStore == null ? null : descriptionStore,
        "Address_Store": addressStore == null ? null : addressStore,
        "Pic_Store": picStore == null ? null : picStore,
        "TID": tid == null ? null : tid,
        "Number_Account": numberAccount == null ? null : numberAccount,
        "Name_Bank": nameBank == null ? null : nameBank,
        "Prompt_pay": promptPay == null ? null : promptPay,
      };
}
