import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:avatar_glow/avatar_glow.dart';
import 'package:project_likeshop_msu/TemplateUI/UI_Button.dart';
import 'package:project_likeshop_msu/utils/colors.dart';

import '../../TemplateUI/UI_Appbar.dart';
import '../../utils/TextStyle.dart';

class Welcome_page extends StatefulWidget {
  const Welcome_page({Key? key}) : super(key: key);

  @override
  State<Welcome_page> createState() => _Welcome_pageState();
}

class _Welcome_pageState extends State<Welcome_page> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: appBar(context),
      backgroundColor: backgroundColor,
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 50),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text(
                    "ยินดีต้อนรับ",
                    style: TextVeryLargeBlackNormal,
                  ),
                ],
              ),

              Container(
                child: Center(
                   // ปรับขนาดเพื่อพอดีกับพื้นที่
                      child: Image.asset('assets/icons/LOGO-MSU-Shop.png')

                ),
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                children: <Widget>[
                  ButtonHomeScreen(context, "ลูกค้า", "/Customer_login"),
                  SizedBox(
                    height: 20,
                  ),
                  ButtonHomeScreen(context, "ร้านค้า", "/Login_Store")
                ],
              )
            ],
          ),
        ),
      ),

    );
  }
}
