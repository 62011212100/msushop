import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:project_likeshop_msu/Widgets/Widget.dart';
import 'package:project_likeshop_msu/utils/colors.dart';
class Sign_Customer extends StatefulWidget {
  const Sign_Customer({Key? key}) : super(key: key);

  @override
  State<Sign_Customer> createState() => _Sign_CustomerState();
}

class _Sign_CustomerState extends State<Sign_Customer> {
  final usernameSignCController = TextEditingController();
  final emaliSignCController = TextEditingController();
  final passwordSignCController = TextEditingController();
  final confirmpasswordSignCController = TextEditingController();

  bool isObsecure = true;
  bool isConfirmObsecure = true;

  String? usernameSignC ;
  String? emaliSignC ;
  String? passwordSignC ;
  String? confirmpasswordSignC ;

  void _setText() {
    setState(() {
      usernameSignC = usernameSignCController.text;
      emaliSignC = emaliSignCController.text;
      passwordSignC = passwordSignCController.text;
      confirmpasswordSignC = confirmpasswordSignCController.text;
    });
  }

  final Color facebookColor = const Color(0xff39579A);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            size: 20,
            color: Colors.black,
          ),
        ),
      ),
      body: ListView(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Center(
                              child: Container(
                                  height: 150,
                                  width: 150,
                                  child: Image.asset(
                                      'assets/icons/LOGO-MSU-Shop.png'))),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Column(
                          children: <Widget>[
                            Text(
                              "สมัครบัญชีผู้ใช้",
                              style: TextStyle(
                                  fontSize: 30, fontWeight: FontWeight.bold),
                            ),
                            // SizedBox(height: 20,),
                            Text(
                              "กรุณากรอกข้อมูลเพื่อทำการสมัครบัญชี",
                              style:
                              TextStyle(fontSize: 15, color: Colors.grey[700]),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 40),
                          child: Column(
                            children: <Widget>[
                              TextFormField(
                                scrollPadding: EdgeInsets.all(10),
                                decoration: InputDecoration(
                                  labelText: 'อีเมลล์',
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  prefixIcon: Icon(Icons.email_outlined),
                                ),
                                controller: emaliSignCController,
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              TextFormField(
                                scrollPadding: EdgeInsets.all(10),
                                decoration: InputDecoration(
                                  labelText: 'ชื่อผู้ใช้',
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  prefixIcon: Icon(Icons.perm_identity_outlined),
                                ),
                                controller: usernameSignCController,
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              TextFormField(
                                //enabled: this.passwordSignC != null && this.passwordSignC!.isNotEmpty,
                                scrollPadding: EdgeInsets.all(10),
                                obscureText: isObsecure,
                                decoration: InputDecoration(
                                  labelText: 'รหัสผ่าน',
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        isObsecure = !isObsecure;
                                      });
                                    },
                                    icon: Icon(
                                      isObsecure
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                    ),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  prefixIcon: Icon(Icons.vpn_key_sharp),
                                ),
                                controller: passwordSignCController,
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              TextFormField(

                                //enabled: this.confirmpasswordSignC != null && this.confirmpasswordSignC!.isNotEmpty,
                                scrollPadding: EdgeInsets.all(10),
                                obscureText: isConfirmObsecure,
                                decoration: InputDecoration(
                                  labelText: 'ยืนยันหัสผ่านอีกครั้ง',
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        isConfirmObsecure = !isConfirmObsecure;
                                      });
                                    },
                                    icon: Icon(
                                      isConfirmObsecure
                                          ? Icons.visibility_off
                                          : Icons.visibility,
                                    ),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  prefixIcon: Icon(Icons.vpn_key_outlined),
                                ),
                                controller: confirmpasswordSignCController,
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Column(
                                children: <Widget>[
                                  MaterialButton(
                                    minWidth: double.infinity,
                                    height: 60,
                                    onPressed: () {
                                      _setText();
                                      Navigator.pushNamed(context, "/Customer_login");
                                      print("Email is :   "+emaliSignCController.text);
                                      print("Username is :  "+usernameSignCController.text);
                                      print("Password is :  "+passwordSignCController.text);
                                      print("ConfirmPassword is  :  "+confirmpasswordSignCController.text);

                                    },
                                    color: ButtonColors,
                                    shape: RoundedRectangleBorder(
                                        side: BorderSide(color: Colors.black),
                                        borderRadius: BorderRadius.circular(30)),
                                    child: Text(
                                      "สมัครบัญชีผู้ใช้",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 18),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ))
              ],
            ),
          ),
        ],
      ),
    );
  }


}
