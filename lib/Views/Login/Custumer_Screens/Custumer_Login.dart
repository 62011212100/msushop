import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:project_likeshop_msu/Function/CheckLogIn.dart';
import 'package:project_likeshop_msu/TemplateUI/UI_Button.dart';
import 'package:project_likeshop_msu/TemplateUI/UI_TextFields.dart';
import 'package:project_likeshop_msu/Widgets/Widget.dart';
import 'package:project_likeshop_msu/utils/colors.dart';

class Customer_login extends StatefulWidget {
  const Customer_login({Key? key}) : super(key: key);

  @override
  State<Customer_login> createState() => _Customer_loginState();
}

class _Customer_loginState extends State<Customer_login> {
  final usernameLoginCController = TextEditingController();
  final passwordLoginCController = TextEditingController();

  String? usernameLoginC;
  String? passwordLoginC;

  void _setText() {
    setState(() {
      usernameLoginC = usernameLoginCController.text;
      passwordLoginC = passwordLoginCController.text;
    });
  }

  bool _isHidden = true;
  bool isObsecure = true;

  final Color facebookColor = const Color(0xff39579A);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            size: 20,
            color: Colors.black,
          ),
        ),
      ),
      body: ListView(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Center(
                          child: Container(
                              height: 150,
                              width: 150,
                              child:
                                  Image.asset('assets/icons/LOGO-MSU-Shop.png'))),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Column(
                      children: <Widget>[
                        Text(
                          "เข้าสู่ระบบ",
                          style:
                              TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                        ),
                        // SizedBox(height: 20,),
                        Text(
                          "กรุณากรอกข้อมูลเพื่อเข้าสู่ระบบ",
                          style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 40),
                      child: Column(
                        children: <Widget>[
                          TFFInputUsername('ชื่อผู้ใช้', Icons.perm_identity_outlined, usernameLoginCController),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormField(
                            scrollPadding: EdgeInsets.all(10),
                            obscureText: isObsecure,
                            decoration: InputDecoration(
                              labelText: 'รหัสผ่าน',
                              suffixIcon: IconButton(
                                onPressed: () {
                                  setState(() {
                                    isObsecure = !isObsecure;
                                  });
                                },
                                icon: Icon(
                                  isObsecure
                                      ? Icons.visibility_off
                                      : Icons.visibility,
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                              ),
                              prefixIcon: Icon(Icons.vpn_key_sharp),
                            ),
                            controller: passwordLoginCController,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Column(

                            children: <Widget>[

                              ButtonScreenLogin(context, "เข้าสู่ระบบ", "/Homepage_Customer", usernameLoginCController, passwordLoginCController),
                              SizedBox(
                                height: 20,
                              ),
                              _textAccount(),
                              SizedBox(
                                height: 10,
                              ),
                              _LoginWithFacebook(),
                              CustomWidgets.socialButtonRect(
                                  'เข้าสู่ระบบด้วย Facebook',
                                  facebookColor,
                                  FontAwesomeIcons.facebookF, onTap: () {
                                Navigator.pushNamed(context, "/Welcome_page");
                                //Fluttertoast.showToast(msg: 'I am facebook');
                              }),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ))
              ],
            ),
          ),
        ],
      ),
    );
  }

  RichText _textAccount() {
    return RichText(
      text: TextSpan(
          text: "หากไม่มีบัญชีผู้ใช้  ",
          children: [
            TextSpan(
              style: TextStyle(color: Colors.deepOrange),
              text: 'สร้างบัญชีผู้ใช้ใหม่',
              recognizer: TapGestureRecognizer()
                ..onTap = () => Navigator.pushNamed(context, "/Sign_Customer"),
            )
          ],
          style: TextStyle(
              color: Colors.black87, fontSize: 14, fontFamily: 'Exo2')),
    );
  }

  RichText _LoginWithFacebook() {
    return RichText(
      text: TextSpan(
          text: "หรือเข้าสู่ระบบโดยใช้ ",
          children: [
            TextSpan(
              style: TextStyle(color: Colors.blue),
              text: 'Facebook',
              recognizer: TapGestureRecognizer()
                ..onTap = () => Navigator.pushNamed(context, "/Welcome_page"),
            )
          ],
          style: TextStyle(
              color: Colors.black87, fontSize: 14, fontFamily: 'Exo2')),
    );
  }
}
