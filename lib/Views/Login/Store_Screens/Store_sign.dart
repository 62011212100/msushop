import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:project_likeshop_msu/Widgets/Widget.dart';
import 'package:project_likeshop_msu/utils/colors.dart';

import '../../../Models/Register/Store/registerStore.dart';
import '../../../baseClient/baseClients.dart';

class Sign_Store extends StatefulWidget {
  const Sign_Store({Key? key}) : super(key: key);

  @override
  State<Sign_Store> createState() => _Sign_StoreState();
}

class _Sign_StoreState extends State<Sign_Store> {
  final usernameSignSController = TextEditingController();
  final emailSignSController = TextEditingController();
  final passwordSignSController = TextEditingController();
  final confirmpasswordSignSController = TextEditingController();

  bool isObsecure = true;
  bool isConfirmObsecure = true;

  String? usernameSignS ;
  String? emailSignS ;
  String? passwordSignS ;
  String? confirmpasswordSignS ;

  void _setText() {
    setState(() {
      usernameSignS = usernameSignSController.text;
      emailSignS = emailSignSController.text;
      passwordSignS = passwordSignSController.text;
      confirmpasswordSignS = confirmpasswordSignSController.text;
    });
  }

  final Color facebookColor = const Color(0xff39579A);
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            size: 20,
            color: Colors.black,
          ),
        ),
      ),

      body: Form(
        key: _formKey,
        child: ListView(
          children: [
            Container(
              height: MediaQuery.of(context).size.height,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Center(
                            child: Container(
                                height: 150,
                                width: 150,
                                child: Image.asset(
                                    'assets/icons/LOGO-MSU-Shop.png'))),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Column(
                        children: <Widget>[
                          Text(
                            "สมัครบัญชีผู้ใช้",
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold),
                          ),
                          // SizedBox(height: 20,),
                          Text(
                            "กรุณากรอกข้อมูลเพื่อทำการสมัครบัญชี",
                            style:
                                TextStyle(fontSize: 15, color: Colors.grey[700]),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 40),
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                              scrollPadding: EdgeInsets.all(10),
                              decoration: InputDecoration(
                                labelText: 'อีเมลล์',
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                prefixIcon: Icon(Icons.email_outlined),
                              ),
                              controller: emailSignSController,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'กรุณากรอกอีเมลล์';
                                }
                                final emailRegex = RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$');
                                if (!emailRegex.hasMatch(value)) {
                                  return 'กรุณากรอกอีเมลล์ให้ถูกต้อง';
                                }
                                return null;
                              },
                            ),

                            SizedBox(
                              height: 20,
                            ),
                            TextFormField(
                              scrollPadding: EdgeInsets.all(10),
                              decoration: InputDecoration(
                                labelText: 'ชื่อผู้ใช้',
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                prefixIcon: Icon(Icons.perm_identity_outlined),
                              ),
                              controller: usernameSignSController,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextFormField(
                              scrollPadding: EdgeInsets.all(10),
                              obscureText: isObsecure,
                              decoration: InputDecoration(
                                labelText: 'รหัสผ่าน',
                                suffixIcon: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      isObsecure = !isObsecure;
                                    });
                                  },
                                  icon: Icon(
                                    isObsecure
                                        ? Icons.visibility_off
                                        : Icons.visibility,
                                  ),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                prefixIcon: Icon(Icons.vpn_key_sharp),
                              ),
                              controller: passwordSignSController,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'โปรดระบุรหัสผ่าน';
                                }
                                if (value != confirmpasswordSignSController.text) {
                                  return 'รหัสผ่านไม่ตรงกัน';
                                }
                                return null;
                              },
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextFormField(
                              scrollPadding: EdgeInsets.all(10),
                              obscureText: isConfirmObsecure,
                              decoration: InputDecoration(
                                labelText: 'ยืนยันหัสผ่านอีกครั้ง',
                                suffixIcon: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      isConfirmObsecure = !isConfirmObsecure;
                                    });
                                  },
                                  icon: Icon(
                                    isConfirmObsecure
                                        ? Icons.visibility_off
                                        : Icons.visibility,
                                  ),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                prefixIcon: Icon(Icons.vpn_key_outlined),
                              ),
                              controller: confirmpasswordSignSController,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'โปรดระบุยืนยันรหัสผ่าน';
                                }
                                if (value != passwordSignSController.text) {
                                  return 'รหัสผ่านไม่ตรงกัน';
                                }
                                return null;
                              },
                            ),

                            SizedBox(
                              height: 20,
                            ),
                            Column(
                              children: <Widget>[
                                MaterialButton(
                                  minWidth: double.infinity,
                                  height: 60,
                                  onPressed: () async {
                                    if (_formKey.currentState!.validate()) {
                                      _formKey.currentState!.save();
                                      var registerStore = RegisterStore(
                                          registerStoreEmail: emailSignSController.text,
                                          registerStoreNameUser:usernameSignSController.text ,
                                          registerStorePassword: passwordSignSController.text

                                      );


                                      var response = await BaseClient().post('/registerStore', registerStore).catchError((err) {});
                                      Navigator.pushNamed(context, "/Login_Store") ;
                                      if (response == null) return;
                                      debugPrint('successful:'+emailSignSController.text);
                                      print("Email is :   "+emailSignSController.text);
                                      print("Username is :  "+usernameSignSController.text);
                                      print("Password is :  "+passwordSignSController.text);
                                      print("ConfirmPassword is  :  "+confirmpasswordSignSController.text);
                                    }
                                  },
                                  color: ButtonColors,
                                  shape: RoundedRectangleBorder(
                                      side: BorderSide(color: Colors.black),
                                      borderRadius: BorderRadius.circular(30)),
                                  child: Text(
                                    "สมัครบัญชีผู้ใช้",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 18),
                                  ),
                                ),

                                SizedBox(
                                  height: 20,
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }


}
