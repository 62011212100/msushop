import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:project_likeshop_msu/Models/Register/Store/loginStore.dart';
import 'package:project_likeshop_msu/Widgets/Widget.dart';
import 'package:project_likeshop_msu/utils/colors.dart';
import 'package:http/http.dart' as http;
import '../../../Models/Register/Store/statusStoreLogin.dart';
import '../../../baseClient/baseClients.dart';

class Login_Store extends StatefulWidget {
  const Login_Store({Key? key}) : super(key: key);

  @override
  State<Login_Store> createState() => _Login_StoreState();

}

class _Login_StoreState extends State<Login_Store> {
  final usernameLoginSController = TextEditingController();
  final passwordLoginSController = TextEditingController();

  String? usernameLoginS;
  String? passwordLoginS;

  bool isObsecure = true;

  final Color facebookColor = const Color(0xff39579A);
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            size: 20,
            color: Colors.black,
          ),
        ),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Center(
                        child: Container(
                            height: 150,
                            width: 150,
                            child:
                                Image.asset('assets/icons/LOGO-MSU-Shop.png'))),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        "เข้าสู่ระบบ",
                        style: TextStyle(
                            fontSize: 30, fontWeight: FontWeight.bold),
                      ),
                      // SizedBox(height: 20,),
                      Text(
                        "กรุณากรอกข้อมูลเพื่อเข้าสู่ระบบ",
                        style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 40),
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          scrollPadding: EdgeInsets.all(10),
                          decoration: InputDecoration(
                            labelText: 'ชื่อผู้ใช้',
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                            prefixIcon: Icon(Icons.perm_identity_outlined),
                          ),
                          controller: usernameLoginSController,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          scrollPadding: EdgeInsets.all(10),
                          obscureText: isObsecure,
                          decoration: InputDecoration(
                            labelText: 'รหัสผ่าน',
                            suffixIcon: IconButton(
                              onPressed: () {
                                setState(() {
                                  isObsecure = !isObsecure;
                                });
                              },
                              icon: Icon(
                                isObsecure
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                              ),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                            prefixIcon: Icon(Icons.vpn_key_sharp),
                          ),
                          controller: passwordLoginSController,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Column(
                          children: <Widget>[
                            MaterialButton(
                              minWidth: double.infinity,
                              height: 60,
                              onPressed: () async {

                                if (_formKey.currentState!.validate()) {
                                  _formKey.currentState!.save();


                                  debugPrint('successful:' + usernameLoginSController.text);
                                  print("Username is :   " +
                                      usernameLoginSController.text);
                                  print("Password is :  " +
                                      passwordLoginSController.text);
                                }
                                loginStore();
                              },
                              color: ButtonColors,
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(color: Colors.black),
                                  borderRadius: BorderRadius.circular(30)),
                              child: Text(
                                "เข้าสู่ระบบ",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 18),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            _textAccount(),
                            SizedBox(
                              height: 10,
                            ),
                            _LoginWithFacebook(),
                            CustomWidgets.socialButtonRect(
                                'เข้าสู่ระบบด้วย Facebook',
                                facebookColor,
                                FontAwesomeIcons.facebookF, onTap: () {
                              Navigator.pushNamed(context, "/Welcome_page");
                              //Fluttertoast.showToast(msg: 'I am facebook');
                            }),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ))
            ],
          ),
        ),
      ),
    );
  }

  Future<void> loginStore() async {
    if (passwordLoginSController.text.isNotEmpty &&
        usernameLoginSController.text.isNotEmpty) {
      var response = await http.post(Uri.parse("https://msushop.comsciproject.net/MSUSHOP_API/index.php/loginStore"),
          body: ({
            'register_Store_NameUser' : usernameLoginSController.text,
            'register_Store_Password' : passwordLoginSController.text
          }));
      if (response.statusCode==200){
        Navigator.pushNamed(context, "/homepages");

      }else{
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("ไม่พบข้อมูลผู้ใช้งาน")));
      }
    }
    else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("ข้อมูลไม่ถูกต้อง")));
    }
  }


  RichText _textAccount() {
    return RichText(
      text: TextSpan(
          text: "หากไม่มีบัญชีผู้ใช้  ",
          children: [
            TextSpan(
              style: TextStyle(color: Colors.deepOrange),
              text: 'สร้างบัญชีผู้ใช้ใหม่',
              recognizer: TapGestureRecognizer()
                ..onTap = () => Navigator.pushNamed(context, "/Sign_Store"),
            )
          ],
          style: TextStyle(
              color: Colors.black87, fontSize: 14, fontFamily: 'Exo2')),
    );
  }

  RichText _LoginWithFacebook() {
    return RichText(
      text: TextSpan(
          text: "หรือเข้าสู่ระบบโดยใช้ ",
          children: [
            TextSpan(
              style: TextStyle(color: Colors.blue),
              text: 'Facebook',
              recognizer: TapGestureRecognizer()
                ..onTap = () => Navigator.pushNamed(context, "/Welcome_page"),
            )
          ],
          style: TextStyle(
              color: Colors.black87, fontSize: 14, fontFamily: 'Exo2')),
    );
  }
}
