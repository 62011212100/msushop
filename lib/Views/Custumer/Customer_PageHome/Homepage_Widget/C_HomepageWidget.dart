import 'package:flutter/material.dart';
import 'package:project_likeshop_msu/utils/colors.dart';
class Customer_Homepage_Widget extends StatefulWidget {
  const Customer_Homepage_Widget({Key? key}) : super(key: key);

  @override
  State<Customer_Homepage_Widget> createState() => _Customer_Homepage_WidgetState();
}

class _Customer_Homepage_WidgetState extends State<Customer_Homepage_Widget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 10),
            child: Padding(
              padding: EdgeInsets.all(16),
              child: TextField(
                decoration: InputDecoration(
                  hintText: 'Search',
                  hintStyle: TextStyle(fontSize: 16),
                  border: OutlineInputBorder(

                    borderRadius: BorderRadius.circular(15),

                    borderSide: BorderSide(

                      width: 0,
                      style: BorderStyle.none,
                    ),

                  ),
                  filled: true,
                  fillColor: Colors.grey[100],
                  contentPadding: EdgeInsets.only(left: 30,),
                  suffixIcon: Padding(
                    padding: EdgeInsets.only(right: 24.0, left: 16.0),
                    child: Icon(
                      Icons.search,
                      color: Colors.black,
                      size: 24,
                    ),
                  ),
                ),
              ),
            ),
          ),

        ],
      ),

    );
  }
}
