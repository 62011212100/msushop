import 'dart:io';

import 'package:flutter/material.dart';
import 'package:project_likeshop_msu/utils/colors.dart';
import 'package:image_picker/image_picker.dart';
class Customer_OtherPageWidget extends StatefulWidget {
  const Customer_OtherPageWidget({Key? key}) : super(key: key);

  @override
  State<Customer_OtherPageWidget> createState() =>
      _Customer_OtherPageWidgetState();
}

class _Customer_OtherPageWidgetState extends State<Customer_OtherPageWidget> {
  File? _image;
  _getFormGallery() async {
    PickedFile? pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
      maxWidth: 1800,
      maxHeight: 1800,
    );

    if (pickedFile != null) {
      setState(() {
        _image = File(pickedFile.path);
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          SizedBox(height: 20,),
          Center(
            child: _image == null
                ? new Container(
              child: new Column(
                children: <Widget>[
                  new CircleAvatar(
                    backgroundColor: Colors.orangeAccent,
                    radius: 50.0,
                  ),
                  //_getFormGallery(),
                 buildContainer(context)
                ],
              ),
            )
                : Container(
              child: new Column(
                children: [
                  new CircleAvatar(
                    backgroundImage: new FileImage(_image!),
                    radius: 100.0,
                  ),
                  //_getFormGallery(),
                  buildContainer(context)

                ],
              ),
            ),
          ),
          ListTitle(context,"/Homepage_Customer","รหัสผู้ใช้","(รหัสผู้ใช้)",Icons.personal_injury_sharp),
          SizedBox(height: 5,),
          ListTitle(context,"/Homepage_Customer","ชื่อผู้ใช้","(ชื่อของผู้ใช้)",Icons.person),
          SizedBox(height: 5,),
          ListTitle(context,"/Homepage_Customer","เบอร์โทร","(เบอร์โทร)",Icons.phone),
          SizedBox(height: 5,),
          ListTitle(context,"/Homepage_Customer","อีเมล์","(อีเมล์)",Icons.email_outlined),
          SizedBox(height: 5,),
          ListTitle(context,"/Homepage_Customer","Facebook","(Facebook)",Icons.facebook),
          SizedBox(height: 5,),
          ListTitle(context,"/Homepage_Customer","ที่อยู่","(ที่อยู่)",Icons.home_work),
        ],
      ),
    );
  }

  Center ListTitle(BuildContext context,String Linkpage,String NameTitle,String NameUser,IconData Iconlist) {
    return Center(
          child: ListTile(
            autofocus: true,
            tileColor: Colors.white,
            selectedTileColor: Colors.grey,
            onTap: () => showDialog<String>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: Text(NameTitle+"ใหม่"),
                content: TextFormField(),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context, 'Cancel'),
                    child: const Text('Cancel'),
                  ),
                  TextButton(
                    onPressed: () => Navigator.pop(context, 'OK'),
                    child: const Text('OK'),
                  ),

                ],
              ),
            ),
            title: Text(NameTitle),
            subtitle: Text(NameUser),
            leading: CircleAvatar(
              backgroundColor: backgroundColorIcon,
              child: Icon(
                 Iconlist,
                color: IconColorsWhite,
              ),
            ),
            trailing: Icon(Icons.edit,color: Colors.orange,),

          ),


        );
  }

  Container buildContainer(BuildContext context) {
    return Container(
      child: new Column(
        children: [
          IconButton(
              icon: Icon(Icons.add_photo_alternate_outlined,
                  color: Color.fromRGBO(250, 120, 186, 1)),
              onPressed: () => showDialog(
                  context: context,
                  builder: (BuildContext context) =>
                      AlertDialog(
                        title: Text('เลือกรูปภาพ'),
                        content: new Container(
                          width: 250,
                          height: 100,
                          child: new Column(
                            children: [
                              // Padding(
                              //   padding:
                              //   const EdgeInsets.fromLTRB(
                              //       10, 0, 0, 20),
                              //   child: SingleChildScrollView(
                              //     scrollDirection:
                              //     Axis.horizontal,
                              //     child: new SizedBox(
                              //       width: 200,
                              //       height: 40,
                              //       child: RaisedButton(
                              //         onPressed: () {
                              //           _getFormGallery();
                              //           Navigator.pop(context);
                              //         },
                              //         child: Text(
                              //             'เลือกจากคลังภาพ'),
                              //       ),
                              //     ),
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                      ))
          ),

        ],
      ),
    );
  }

}
