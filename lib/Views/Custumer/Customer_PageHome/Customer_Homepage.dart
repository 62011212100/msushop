import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:project_likeshop_msu/Views/Store/Store_PageNotifation/Store_Notification.dart';
import 'package:project_likeshop_msu/utils/colors.dart';
import 'Homepage_Widget/C_HistoryWidget.dart';
import 'Homepage_Widget/C_HomepageWidget.dart';
import 'Homepage_Widget/C_OtherPageWidget.dart';
import 'Homepage_Widget/C_orderpageWidget.dart';


class Homepage_Customer extends StatefulWidget {
  const Homepage_Customer({Key? key}) : super(key: key);

  @override
  State<Homepage_Customer> createState() => _Homepage_CustomerState();
}

const _kPages = <String, IconData>{
  'หน้าแรก': Icons.home,
  'คำสั่งซื้อ': Icons.store_outlined,
  'ประวัติ': Icons.history,
  'อื่นๆ': Icons.segment,
};

class _Homepage_CustomerState extends State<Homepage_Customer> {
  int _TabIndex = 0; // amount index
  TabStyle _tabStyle = TabStyle.react; // style Tab

  @override
  Widget build(BuildContext context) {
    //setting show page
    final _kTabPages = <Widget>[
      Container(child: Customer_Homepage_Widget()), //Page home by Buttom bar
      Container(child: Customer_orderpageWidget()), //Page order by Buttom bar
      Container(child: Customer_HistoryWidget()), //Page history by Buttom bar
      Container(child: Customer_OtherPageWidget()), //Page profile by Buttom bar
    ];

    assert(_kTabPages.length == _kPages.length); // check Bottom bar and Page

    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppbarColors,
        leading: const Icon(Icons.tag_faces),
        title: const Text("ศูนย์รวมอาหา มมส"),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.notifications),
            onPressed: () {
              Navigator.pop(context);
              //Navigator.pushNamed(context, "/ProfilePageWidget");
            },
          ),
          IconButton(
            icon: const Icon(Icons.logout),
            onPressed: () {
              Navigator.pushNamed(context, "/Customer_login");
            },
          ),
        ],
      ),
      body: _kTabPages[_TabIndex],
      bottomNavigationBar: ConvexAppBar.badge(
        const <int, dynamic>{},
        style: _tabStyle,
        backgroundColor: ColorsbottomNavigationBar,
        items: <TabItem>[
          for (final entry in _kPages.entries)
            TabItem(icon: entry.value, title: entry.key),
        ],
        onTap: (int index) {
          setState(() {
            _TabIndex = index;
          });
        },
      ),
    );
  }
}
