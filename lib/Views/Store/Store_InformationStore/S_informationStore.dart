import 'package:flutter/material.dart';

class store_information extends StatefulWidget {
  const store_information({Key? key}) : super(key: key);

  @override
  _store_informationState createState() => _store_informationState();
}

class _store_informationState extends State<store_information> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        leading: const Icon(Icons.store_mall_directory_outlined),
        title: const Text("ข้อมูลร้านค้า"),
        actions: <Widget>[
          IconButton(
              icon: const Icon(Icons.close),
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, "/homepages");
              }
          ),
        ],
      ),
      body: Container(
      ),);
  }
}
