import 'package:flutter/material.dart';
class S_user_manual extends StatefulWidget {
  const S_user_manual({Key? key}) : super(key: key);

  @override
  _S_user_manualState createState() => _S_user_manualState();
}

class _S_user_manualState extends State<S_user_manual> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        leading: const Icon(Icons.menu_book_sharp),
        title: const Text("คู่มือการใช้งาน"),
        actions: <Widget>[
          IconButton(
              icon: const Icon(Icons.close),
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, "/homepages");
              }
          ),
        ],
      ),
      body: Container(
      ),);
  }
}

