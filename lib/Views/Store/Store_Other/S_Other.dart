import 'package:flutter/material.dart';
import 'package:project_likeshop_msu/Example/ex3.dart';
class S_Other extends StatefulWidget {
  const S_Other({Key? key}) : super(key: key);

  @override
  _S_OtherState createState() => _S_OtherState();
}

class _S_OtherState extends State<S_Other> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        leading: const Icon(Icons.report),
        title: const Text("อื่นๆ"),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.close),
            onPressed: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, "/homepages");
            }
          ),
        ],
      ),
      body: Container(
      ),);
  }
}
