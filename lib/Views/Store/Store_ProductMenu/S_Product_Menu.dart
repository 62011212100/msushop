import 'package:flutter/material.dart';
class Product_Menu extends StatefulWidget {
  const Product_Menu({Key? key}) : super(key: key);

  @override
  _Product_MenuState createState() => _Product_MenuState();
}

class _Product_MenuState extends State<Product_Menu> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        leading: const Icon(Icons.menu_book_sharp),
        title: const Text("เมนูสินค้า"),
        actions: <Widget>[
          IconButton(
              icon: const Icon(Icons.close),
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, "/homepages");
              }
          ),
        ],
      ),
      body: Container(
      ),);
  }
}
