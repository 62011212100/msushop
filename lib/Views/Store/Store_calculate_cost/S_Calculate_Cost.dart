import 'package:flutter/material.dart';

class S_Calculate_Cost extends StatefulWidget {
  const S_Calculate_Cost({Key? key}) : super(key: key);

  @override
  _S_Calculate_CostState createState() => _S_Calculate_CostState();
}

class _S_Calculate_CostState extends State<S_Calculate_Cost> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        leading: const Icon(Icons.monetization_on_outlined),
        title: const Text("คำนวณค่าใช้จ่าย"),
        actions: <Widget>[
          IconButton(
              icon: const Icon(Icons.close),
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, "/homepages");
              }
          ),
        ],
      ),
      body: Container(
      ),);
  }
}
