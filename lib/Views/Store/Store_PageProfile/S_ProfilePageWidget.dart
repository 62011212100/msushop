import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_likeshop_msu/Example/ex2.dart';

class ProfilePageWidget extends StatefulWidget {
  const ProfilePageWidget({Key? key}) : super(key: key);

  @override
  _ProfilePageWidgetState createState() => _ProfilePageWidgetState();
}

class _ProfilePageWidgetState extends State<ProfilePageWidget> {
  bool _isReadonly = true;
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(0.0),
              //ไอคอนและข้อความหัวข้อ
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      Icons.person_pin,
                      size: 60,
                      color: Colors.black,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextSettingPage("ข้อมูลส่วนตัว", 25.0,
                        FontWeight.bold, FontStyle.normal, Colors.black),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                elevation: 15.0,
                color: Colors.white,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextSettingPage(
                                "รายละเอียดข้อมูลส่วนตัว",
                                20,
                                FontWeight.bold,
                                FontStyle.normal,
                                Colors.black),
                          ),
                          CardData("รหัสผู้ใช้ :", "(รหัสผู้ใช้)"),
                          CardData("ชื่อผู้ใช้ :", "(ชื่อผู้ใช้)"),
                          CardData("เบอร์โทรร้านค้า :", "(เบอร์โทรร้านค้า)"),
                          CardData("อีเมล์ :", "(อีเมล์)"),
                          CardData("Facebook :", "(Facebook)"),
                          CardData("ที่อยู่ :", "(ที่อยู่)"),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: buildElevatedButton("แก้ไข", Colors.black),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Card(
              color: Colors.white,
              elevation: 15.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          Icons.settings,
                          size: 55,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextSettingPage("ตั้งค่า", 30.0, FontWeight.bold,
                            FontStyle.normal, Colors.black),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        IconButton(
                          icon: const Icon(Icons.keyboard_arrow_right),
                          color: Colors.black,
                          onPressed: () {},
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  Card buildCard(IconData MenuIcon, String PageIcon) {
    return Card(
      elevation: 10.0,
      color: Colors.grey,
      child: IconButton(
        iconSize: 40.0,
        icon: Icon(
          MenuIcon,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.pop(context);
          Navigator.pushNamed(context, PageIcon);
          // Navigator.pushNamed(context,'/notifications');
        },
      ),
    );
  }

  Padding CardData(String TitleData, String DataList) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: Container(
          decoration: (BoxDecoration(
              color: Colors.white,
              border: Border.all(width: 5, color: Colors.white),
              borderRadius: BorderRadius.circular(40),
              gradient: const LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [
                    Colors.grey,
                    Colors.grey,
                  ]))),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        (TitleData),
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Row(
                          children: [
                            Text(
                              (DataList),
                              textAlign: TextAlign.left,
                              style:
                                  TextStyle(fontSize: 18, color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }

  Text TextSettingPage(String TextName, double SizeFont, FontWeight weight,
      FontStyle StyleFont, Color color) {
    return Text((TextName),
        style: TextStyle(
            fontSize: SizeFont,
            fontWeight: weight,
            fontStyle: StyleFont,
            color: color));
  }

  ElevatedButton buildElevatedButton(String TextButtons, Color colorsset) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(primary: colorsset, elevation: 20.0)
          .copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
      onPressed: () {
        showDialog<String>(
          context: context,
          builder: (BuildContext context) => SingleChildScrollView(
            child: AlertDialog(
              title: const Text('Dialog title'),
              actions: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      TextFieldInDialog("รหัสผู้ใช้"),
                      TextFieldInDialog("ชื่อผู้ใช้"),
                      TextFieldInDialog("เบอร์โทรร้านค้า"),
                      TextFieldInDialog("อีเมล์"),
                      TextFieldInDialog("Facebook"),
                      TextFieldInDialog("ที่อยู่"),
                    ],
                  ),
                ),

                TextButton(
                  onPressed: () => Navigator.pop(context, 'Cancel'),
                  child: const Text('Cancel'),
                ),
                TextButton(
                  onPressed: () => Navigator.pop(context, 'OK'),
                  child: const Text('OK'),
                ),
              ],
            ),
          ),
        ).then((returnVal) {
          if (returnVal != null) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text('You clicked: $returnVal'),
                action: SnackBarAction(label: 'OK', onPressed: () {}),
              ),
            );
          }
        });
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // Container(
          //
          //   child: Padding(
          //     padding: const EdgeInsets.all(8.0),
          //     child: Icon(Icons.edit,color: Colors.black,),
          //   ),
          //
          // ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextSettingPage(TextButtons, 25, FontWeight.bold,
                FontStyle.normal, Colors.white),
          ),
        ],
      ),
    );
  }

  TextField TextFieldInDialog(String TextName) {
    return TextField(
        decoration: InputDecoration(
      labelText: TextName,
      //prefixIcon: Icon(Icons.s),
    ));
  }
}
