import 'package:group_list_view/group_list_view.dart';
import 'package:flutter/material.dart';
import 'package:project_likeshop_msu/setting/helper_Style.dart';

class Nontifications_Widget extends StatefulWidget {
  const Nontifications_Widget({Key? key}) : super(key: key);

  @override
  State<Nontifications_Widget> createState() => _Nontifications_WidgetState();
}

class _Nontifications_WidgetState extends State<Nontifications_Widget> {
  List<String> titles = [
    'ปิดการให้บริการ',
    'อัพเดทฟังชั่นใหม่',
    'กิจกรรมวันนี้'
  ];
  List<String> details = [
    'ปิดการให้บริการ',
    'อัพเดทฟังชั่นใหม่',
    'กิจกรรมวันนี้'
  ];

  @override
  Widget build(BuildContext context) {
    return _myListView();
  }

  Widget _myListView() {
    return ListView.builder(
      itemCount: titles.length,
      itemBuilder: (context, index) {
        final item = titles[index];
        final details_NFT =details[index];
        return Card(
          child: ListTile(
            title: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(Icons.new_releases),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(item),
                ),


              ],
            ),



            onTap: () {
              showDialog<String>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  title: Text(item),
                  content: Text("รายละเอียด"+details_NFT),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context, 'ตกลง'),
                      child: const Text('ตกลง'),
                    ),
                  ],
                ),
              );
            },
            onLongPress: () {
              //                            <-- onLongPress
              setState(() {
                titles.removeAt(index);
              });
            },
          ),
        );
      },
    );
  }
}
