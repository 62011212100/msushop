import 'package:flutter/material.dart';

class HistoryPageWidget extends StatefulWidget {
  const HistoryPageWidget({Key? key}) : super(key: key);

  @override
  _HistoryPageWidgetState createState() => _HistoryPageWidgetState();
}

class _HistoryPageWidgetState extends State<HistoryPageWidget> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Row(
                  children: <Widget>[
                    Icon(
                      Icons.history,
                      size: 60.0,
                      color: Colors.black,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextSetting("ประวัติรายการอาหาร", 25.0,
                          FontWeight.bold, FontStyle.normal, Colors.black),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        ListDateOrder("รายการวันนี้",),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: CardOrder(
            "(รหัส)",
            "(ชื่อลูกค้า)",
            "(สถานะ)",
            "(ราคา)",
            "(สถานะ)",
          ),
        ),
        ListDateOrder("วันที่",),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: CardOrder(
            "(รหัส)",
            "(ชื่อลูกค้า)",
            "(สถานะ)",
            "(ราคา)",
            "(สถานะ)",
          ),
        ),
        ListDateOrder("วันที่",),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: CardOrder(
            "(รหัส)",
            "(ชื่อลูกค้า)",
            "(สถานะ)",
            "(ราคา)",
            "(สถานะ)",
          ),
        ),
      ],
    );
  }

  Column ListDateOrder(String DateList) {
    return Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: SizedBox(child: Divider(color: Colors.black)),
          ),
          DateOrder(DateList),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Divider(color: Colors.black),
          ),
        ],
      );
  }

  Padding DateOrder(String Dateorders) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                TextSetting(Dateorders, 25.0, FontWeight.normal,
                    FontStyle.normal, Colors.black),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(1.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextListOrder("เสร็จสิ้น", "?"),
                TextListOrder("ไม่เสร็จสิ้น", "?"),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Padding TextListOrder(String status_finish, String Amount_Order) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              TextSetting(status_finish, 15.0, FontWeight.normal,
                  FontStyle.normal, Colors.black),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(3.0),
            child: TextSetting(Amount_Order, 20.0, FontWeight.bold,
                FontStyle.normal, Colors.black),
          ),
          TextSetting("รายการ", 15.0, FontWeight.normal, FontStyle.normal,
              Colors.black),
        ],
      ),
    );
  }

  Card CardOrder(String CodeOrder, String NameCustomer, String payment_status,
      String Amount_price, String Order_status) {
    return Card(
      color: Colors.grey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(1.0),
                      child: TextSetting("รหัสออเดอร์", 15.0, FontWeight.bold,
                          FontStyle.normal, Colors.white),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(1.0),
                      child: TextSetting(CodeOrder, 15.0, FontWeight.bold,
                          FontStyle.normal, Colors.white),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(1.0),
                      child: TextSetting(NameCustomer, 15.0, FontWeight.bold,
                          FontStyle.normal, Colors.white),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: TextSetting(
                          ("การชำระเงิน : " + payment_status),
                          15.0,
                          FontWeight.normal,
                          FontStyle.normal,
                          Colors.white),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: TextSetting(("ราคารวม : " + Amount_price), 15.0,
                          FontWeight.normal, FontStyle.normal, Colors.white),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(1.0),
                          child: TextSettingPage("รายละเอียด", 15.0,
                              FontWeight.bold, FontStyle.normal, Colors.white),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(1.0),
                          child: TextSettingPage(
                            "ตรวจสอบสลิป",
                            15.0,
                            FontWeight.bold,
                            FontStyle.normal,
                            Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: buildElevatedButton("ตกลง", Colors.white),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: buildElevatedButton("ยกเลิก", Colors.white),
                    ),
                  ])
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextSetting(("สถานะออเดอร์  :" + Order_status), 15.0,
                        FontWeight.normal, FontStyle.normal, Colors.white),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child:
                        buildElevatedButton("เสร็จสิ้นออเดอร์", Colors.white),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  ElevatedButton buildElevatedButton(String TextButtons, Color colorsset) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(primary: colorsset, elevation: 20.0)
          .copyWith(elevation: ButtonStyleButton.allOrNull(0.0)),
      onPressed: () {},
      child: TextSetting(
          TextButtons, 15, FontWeight.bold, FontStyle.normal, Colors.black),
    );
  }

  Text TextSetting(String TextName, double SizeFont, FontWeight weight,
      FontStyle StyleFont, Color color) {
    return Text((TextName),
        style: TextStyle(
            fontSize: SizeFont,
            fontWeight: weight,
            fontStyle: StyleFont,
            color: color));
  }

  Text TextSettingPage(String TextName, double SizeFont, FontWeight weight,
      FontStyle StyleFont, Color color) {
    return Text((TextName),
        style: TextStyle(
            fontSize: SizeFont,
            fontWeight: weight,
            fontStyle: StyleFont,
            color: color,
            decoration: TextDecoration.underline));
  }
}
