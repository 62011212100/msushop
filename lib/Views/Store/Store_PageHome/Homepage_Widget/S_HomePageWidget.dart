import 'package:flutter/material.dart';
import 'package:project_likeshop_msu/setting/helper_Style.dart';
import 'package:project_likeshop_msu/utils/colors.dart';

import '../../../../utils/TextStyle.dart';

class S_HomepageWidget extends StatefulWidget {

  const S_HomepageWidget({Key? key}) : super(key: key);

  @override
  _S_HomepageWidgetState createState() => _S_HomepageWidgetState();
}

class _S_HomepageWidgetState extends State<S_HomepageWidget> {
  @override
  void initState() {
    super.initState();
  }
  bool _value = false; // สถานะเริ่มต้นของ Switch
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      child: Column(children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          //mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              "สวัสดี\t [ ชื่อเจ้าของ ]",
              style: TextVeryLargeBlackNormal,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "ชื่อร้าน\t [ ชื่อร้าน ]",
                  style: TextVeryLargeBlackNormal,
                ),
                Switch(
                  value: _value, // กำหนดค่า value ของ Switch
                  onChanged: (value) {
                    setState(() {
                      _value = value; // เมื่อมีการเปลี่ยนสถานะของ Switch ให้อัพเดตค่า _value
                    });
                  },
                  activeTrackColor: Colors.lightGreenAccent, // สีเส้นตัวเลื่อนเมื่อเปิดสถานะ
                  activeColor: Colors.green, // สีตัวเลื่อนเมื่อเปิดสถานะ
                ),
               /* SwitchListTile(
                  value: _value,
                  onChanged: (value) {
                    setState(() {
                      _value = value;
                    });
                  },
                  activeColor: Colors.green,
                  inactiveTrackColor: Colors.grey,
                  activeTrackColor: Colors.lightGreenAccent,
                  secondary: _value ? Icon(Icons.toggle_on) : Icon(Icons.toggle_off), // กำหนดไอคอนแสดงสถานะของ Switch
                )*/
              ],

            ),
          ],
        ),
       /* Column(),*/
       /* ListView(
          children: [Row()],
        ),*/
      ]),
    )));
  }

  Padding TextButtonPage(String textbuttonpage) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextSetting(textbuttonpage, 18.0, FontWeight.normal,
            FontStyle.normal, Colors.black));
  }

  Card buildCard(IconData MenuIcon, String PageIcon) {
    return Card(
      elevation: 10.0,
      color: ThemeIconColors,
      child: IconButton(
        iconSize: 40.0,
        icon: Icon(
          MenuIcon,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.pop(context);
          Navigator.pushNamed(context, PageIcon);
          // Navigator.pushNamed(context,'/notifications');
        },
      ),
    );
  }

  Text TextSetting(String TextName, double SizeFont, FontWeight weight,
      FontStyle StyleFont, Color color) {
    return Text((TextName),
        style: TextStyle(
            fontSize: SizeFont,
            fontWeight: weight,
            fontStyle: StyleFont,
            color: color));
  }
}
