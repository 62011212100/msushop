import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:project_likeshop_msu/Views/Store/Store_PageHistory/S_HistoryWidget.dart';
import 'package:project_likeshop_msu/Views/Store/Store_PageNotifation/Store_Notification.dart';
import 'package:project_likeshop_msu/Views/Store/Store_PageOrder/S_orderpageWidget.dart';
import 'package:project_likeshop_msu/Views/Store/Store_PageProfile/S_ProfilePageWidget.dart';
import 'package:project_likeshop_msu/utils/colors.dart';
import 'Homepage_Widget/S_HomePageWidget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class homepages extends StatefulWidget {
  const homepages({Key? key}) : super(key: key);

  @override
  _homepagesState createState() => _homepagesState();
}

const _kPages = <String, IconData>{
  'หน้าแรก': Icons.home,
  'คำสั่งซื้อ': Icons.store_outlined,
  'ประวัติ': Icons.history,
  'โปรไฟล์': Icons.person_rounded,
};

class _homepagesState extends State<homepages> {
  int _TabIndex = 0; // amount index
  TabStyle _tabStyle = TabStyle.react; // style Tab

  void _logout(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
    Navigator.popUntil(context, (route) => route.isFirst);
  }

  @override
  Widget build(BuildContext context) {
    //setting show page
    final _kTabPages = <Widget>[
      Container(child: S_HomepageWidget()), //Page home by Buttom bar
      Container(child: OrderPageWidget()), //Page order by Buttom bar
      Container(child: HistoryPageWidget()), //Page history by Buttom bar
      Container(child: ProfilePageWidget()), //Page profile by Buttom bar
    ];

    assert(_kTabPages.length == _kPages.length); // check Bottom bar and Page

    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppbarColors,
        leading: const Icon(Icons.tag_faces),
        title: const Text("MSU LIKESHOP"),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.notifications),
            onPressed: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) => new notifications()),
              ).then((value) => null);
            },
          ),
          IconButton(
            icon: const Icon(Icons.logout),
            onPressed: () {
              _logout(context);

            },
          ),
        ],
      ),
      body: _kTabPages[_TabIndex],
      bottomNavigationBar: ConvexAppBar.badge(
        const <int, dynamic>{},
        style: _tabStyle,
        backgroundColor: ColorsbottomNavigationBar,
        items: <TabItem>[
          for (final entry in _kPages.entries)
            TabItem(icon: entry.value, title: entry.key),
        ],
        onTap: (int index) {
          setState(() {
            _TabIndex = index;
          });
        },
      ),
    );
  }
}
