import 'package:flutter/material.dart';
class ReportUser extends StatefulWidget {
  const ReportUser({Key? key}) : super(key: key);

  @override
  _ReportUserState createState() => _ReportUserState();
}

class _ReportUserState extends State<ReportUser> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        leading: const Icon(Icons.warning_amber),
        title: const Text("รายงานผู้ใช้"),
        actions: <Widget>[
          IconButton(
              icon: const Icon(Icons.close),
              onPressed: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, "/homepages");
              }
          ),
        ],
      ),
      body: Container(
      ),);
  }
}
