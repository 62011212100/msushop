import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

TextFormField TFFInputUsername (String TextFieldName ,IconData IconName,TextEditingController usernameLoginCController  ) =>  TextFormField(
  scrollPadding: EdgeInsets.all(10),
  decoration: InputDecoration(
    labelText: TextFieldName,
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(30),
    ),
    prefixIcon: Icon(IconName),

  ),
  controller: Username(usernameLoginCController)

);

TextEditingController Username (TextEditingController usernameLoginCController ){
  return usernameLoginCController;
}

TextFormField TFFInputPassWord  (String TextFieldName ,IconData IconName,TextEditingController passwordLoginCController ,bool isObsecure) =>  TextFormField(
  scrollPadding: EdgeInsets.all(10),
  obscureText: isObsecure,
  decoration: InputDecoration(
    labelText: 'รหัสผ่าน',
    suffixIcon: IconButton(
      onPressed: () {
        /*setState(() {
          isObsecure = !isObsecure;
        });*/
      },
      icon: Icon(
        isObsecure
            ? Icons.visibility_off
            : Icons.visibility,
      ),
    ),
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(30),
    ),
    prefixIcon: Icon(Icons.vpn_key_sharp),
  ),
  controller: passwordLoginCController,
);



TextEditingController Password (TextEditingController passwordLoginCController ){
  return passwordLoginCController;
}
