import 'package:flutter/material.dart';

import '../utils/colors.dart';


AppBar appBar(BuildContext context) =>
AppBar(
  backgroundColor: primaryColor,
  leading: const Icon(Icons.report),
  title: const Text("อื่นๆ"),
  actions: <Widget>[
    IconButton(
        icon: const Icon(Icons.close),
        onPressed: () {
          Navigator.pop(context);
          Navigator.pushNamed(context, "/homepages");
        }
    ),
  ],
);
