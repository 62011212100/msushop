import 'package:flutter/material.dart';
import '../Function/CheckLogIn.dart';
import '../utils/TextStyle.dart';

MaterialButton ButtonMaterial(
        BuildContext context,
        String TextNameButton,
        String NamePageToClickNext,
        double BoarderCount,
        double Height,
        double width,
        Color colors,
        double evelationCount) =>
    MaterialButton(
      minWidth: width,
      height: Height,
      elevation: evelationCount,
      onPressed: () {
        Navigator.pushNamed(context, NamePageToClickNext);
      },
      color: colors,
      shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.black),
          borderRadius: BorderRadius.circular(BoarderCount)),
      child: Text(TextNameButton, style: TextVeryLargeBlackNormal),
    );

MaterialButton ButtonHomeScreen(
  BuildContext context,
  String TextNameButton,
  String NamePageToClickNext,
) =>
    ButtonMaterial(context, TextNameButton, NamePageToClickNext, 50, 60,
        double.infinity, Colors.orangeAccent, 10);

MaterialButton ButtonScreenLogin(
        BuildContext context,
        String TextNameButton,
        String NamePageToClickNext,
        TextEditingController username,
        TextEditingController password) =>
    MaterialButton(
      minWidth: double.infinity,
      height: 60,
      elevation: 10,
      onPressed: () {
        bool logInChecking = false;
        logInChecking = CheckingLogin(username, password);
        if (logInChecking == true) {
          print("ข้อมูลการล็อกอิน ถูกต้อง");
          Navigator.pushNamed(context, "/Homepage_Customer");
        } else {
          print("ข้อมูลการล็อกอิน ไม่ถูกต้อง");
        }
      },
      color: Colors.orangeAccent,
      shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.black),
          borderRadius: BorderRadius.circular(50)),
      child: Text(TextNameButton, style: TextVeryLargeBlackNormal),
    );

MaterialButton ButtonScreenSignIn(
  BuildContext context,
  String TextNameButton,
  String NamePageToClickNext,
) =>
    ButtonMaterial(context, TextNameButton, NamePageToClickNext, 50, 60,
        double.infinity, Colors.orangeAccent, 10);
