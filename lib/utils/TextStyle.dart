import 'dart:ui';
import 'package:flutter/material.dart';

import 'colors.dart';


String font = "Itim" ;
double? fontVerylarge = 18;
double? fontLarge = 16;
double? fontMedium = 14;
double? fontNormal = 12;
double? fontSmall = 10;
double? fontVerysmall = 8;

FontWeight fontWeightbold = FontWeight.bold;
FontWeight fontWeightNormal = FontWeight.normal;

TextStyle TextVeryLargeBlackBold = TextStyle(color: TextBlack,fontWeight: fontWeightbold ,fontSize: fontVerylarge ,fontFamily: font);
TextStyle TextfontLargeBlackBold = TextStyle(color: TextBlack,fontWeight: fontWeightbold ,fontSize: fontLarge ,fontFamily: font);
TextStyle TextfontMediumBlackBold = TextStyle(color: TextBlack,fontWeight: fontWeightbold ,fontSize: fontMedium ,fontFamily: font);
TextStyle TextfontNormalBlackBold = TextStyle(color: TextBlack,fontWeight: fontWeightbold ,fontSize: fontNormal ,fontFamily: font);
TextStyle TextfontSmallBlackBold = TextStyle(color: TextBlack,fontWeight: fontWeightbold ,fontSize: fontSmall ,fontFamily: font);
TextStyle TextfontVerysmallBlackBold = TextStyle(color: TextBlack,fontWeight: fontWeightbold ,fontSize: fontVerysmall ,fontFamily: font);


TextStyle TextVeryLargeBlackNormal = TextStyle(color: TextBlack,fontWeight: fontWeightNormal ,fontSize: fontVerylarge ,fontFamily: font);
TextStyle TextfontLargeBlackNormal = TextStyle(color: TextBlack,fontWeight: fontWeightNormal ,fontSize: fontLarge ,fontFamily: font);
TextStyle TextfontMediumBlackNormal = TextStyle(color: TextBlack,fontWeight: fontWeightNormal ,fontSize: fontMedium ,fontFamily: font);
TextStyle TextfontNormalBlackNormal = TextStyle(color: TextBlack,fontWeight: fontWeightNormal ,fontSize: fontNormal ,fontFamily: font);
TextStyle TextfontSmallBlackNormal = TextStyle(color: TextBlack,fontWeight: fontWeightNormal ,fontSize: fontSmall ,fontFamily: font);
TextStyle TextfontVerysmallBlackNormal = TextStyle(color: TextBlack,fontWeight: fontWeightNormal ,fontSize: fontVerysmall ,fontFamily: font);


TextStyle TextVeryLargeWhiteBold = TextStyle(color: TextWhite,fontWeight: fontWeightbold ,fontSize: fontVerylarge ,fontFamily: font);
TextStyle TextfontLargeWhiteBold = TextStyle(color: TextWhite,fontWeight: fontWeightbold ,fontSize: fontLarge ,fontFamily: font);
TextStyle TextfontMediumWhiteBold = TextStyle(color: TextWhite,fontWeight: fontWeightbold ,fontSize: fontMedium ,fontFamily: font);
TextStyle TextfontNormalWhiteBold = TextStyle(color: TextWhite,fontWeight: fontWeightbold ,fontSize: fontNormal ,fontFamily: font);
TextStyle TextfontSmallWhiteBold = TextStyle(color: TextWhite,fontWeight: fontWeightbold ,fontSize: fontSmall ,fontFamily: font);
TextStyle TextfontVerysmallWhiteBold = TextStyle(color: TextWhite,fontWeight: fontWeightbold ,fontSize: fontVerysmall ,fontFamily: font);


TextStyle TextVeryLargeWhiteNormal = TextStyle(color: TextWhite,fontWeight: fontWeightNormal ,fontSize: fontVerylarge ,fontFamily: font);
TextStyle TextfontLargeWhiteNormal = TextStyle(color: TextWhite,fontWeight: fontWeightNormal ,fontSize: fontLarge ,fontFamily: font);
TextStyle TextfontMediumWhiteNormal = TextStyle(color: TextWhite,fontWeight: fontWeightNormal ,fontSize: fontMedium ,fontFamily: font);
TextStyle TextfontNormalWhiteNormal = TextStyle(color: TextWhite,fontWeight: fontWeightNormal ,fontSize: fontNormal ,fontFamily: font);
TextStyle TextfontSmallWhiteNormal = TextStyle(color: TextWhite,fontWeight: fontWeightNormal ,fontSize: fontSmall ,fontFamily: font);
TextStyle TextfontVerysmallWhiteNormal = TextStyle(color: TextWhite,fontWeight: fontWeightNormal ,fontSize: fontVerysmall ,fontFamily: font);











TextStyle TextTitleBlack = TextStyle(color: Colors.black,fontWeight: FontWeight.w100 ,fontSize: 18);
TextStyle TextDescriptionBlack = TextStyle(color: Colors.black,fontWeight: FontWeight.w100 ,fontSize: 16);
//TextStyle TextBlack = TextStyle(color: Colors.black,fontWeight: FontWeight.w100 ,fontSize: 18);
TextStyle TextBTLargeBlack = TextStyle(color: Colors.black,fontWeight: FontWeight.w600 ,fontSize: 25,fontFamily: font);












/*

abstract class MyTextStyle {
  TextStyle get textStyle;

}

class TextTitleBlack2 extends MyTextStyle {
  @override
  TextStyle get textStyle => TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.w100,
    fontSize: 18,
  );
}

class TextDescriptionBlack2 extends MyTextStyle {
  @override
  TextStyle get textStyle => TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.w100,
    fontSize: 16,
  );
}
*/
