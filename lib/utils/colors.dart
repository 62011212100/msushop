import 'dart:ui';
import 'package:flutter/material.dart';
import 'dart:math' as math;

Color colorBlack =Colors.black;
Color colorCurve = Color.fromRGBO(97, 10, 165, 0.8);
Color colorCurveSecondary = Color.fromRGBO(97, 10, 155, 0.6);
/*Color backgroundColor =Colors.grey.shade200;*/
Color textPrimaryColor =Colors.black87;
Color ButtonColors =Colors.orangeAccent;
Color ColorsbottomNavigationBar =Colors.orangeAccent;
Color AppbarColors = Colors.orangeAccent;
Color ThemeIconColors = Colors.amber;
Color LinecardColors = Colors.orange;
Color backgroundColorIcon =Colors.amberAccent;
Color IconColorsWhite = Colors.white;
Color IconColorsBlack = Colors.black;

//textColors
Color textPrimaryLightColor = Colors.white;
Color textPrimaryDarkColor = Colors.black;
Color textSecondaryLightColor = Colors.black87;
Color textSecondary54 = Colors.black54;
Color textSecondaryDarkColor = Colors.blue;
Color hintTextColor = Colors.white30;
Color bucketDialogueUserColor = Colors.red;
Color disabledTextColour = Colors.black54;
Color placeHolderColor = Colors.black26;
Color dividerColor = Colors.black26;
/*Color primaryColor = Color(0xFF0080FF);*/
Color accentColor = Color(0xFF00C8FF);

Color primaryColor2 = Color(0xFF0033CC);
Color accentColor2 = Color(0xFFE1E6EA);



/*// เริ่มต้นเซตค่าสี
Color Primary = Colors.orangeAccent;
Color OnPrimary = Colors.white;
Color Secondary = Colors.deepOrange;
Color OnSecondary = Colors.deepOrange;
Color Background = Colors.white;
Color OnBackground = Colors.orangeAccent;
Color Backgroundtransparent = Colors.transparent;*/

// เริ่มต้นเซตค่าสี
Color primaryColor = Colors.blue;
Color onPrimaryColor = Colors.white;
Color? secondaryColor = Colors.grey[800];
Color onSecondaryColor = Colors.white;
Color backgroundColor = Colors.white;
Color onBackgroundColor = Colors.blue;

Color TextBlack = Colors.black;
Color TextWhite = Colors.white;



