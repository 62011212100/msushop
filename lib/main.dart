import 'package:flutter/material.dart';
import 'package:project_likeshop_msu/Views/Store/Store_PageHome/Store_Homepage.dart';
import 'package:project_likeshop_msu/Views/Store/Store_PageNotifation/Store_Notification.dart';
import 'Views/Custumer/Customer_PageHome/Customer_Homepage.dart';
import 'Views/Login/Custumer_Screens/Custumer_Login.dart';
import 'Views/Login/Custumer_Screens/Custumer_sign.dart';
import 'Views/Login/Store_Screens/Store_Login.dart';
import 'Views/Login/Store_Screens/Store_sign.dart';
import 'Views/Login/Welcome_pages.dart';
import 'Views/Store/Store_InformationStore/S_informationStore.dart';
import 'Views/Store/Store_Other/S_Other.dart';
import 'Views/Store/Store_PageHome/Homepage_Widget/S_HomePageWidget.dart';
import 'Views/Store/Store_PageProfile/S_ProfilePageWidget.dart';
import 'Views/Store/Store_ProductMenu/S_Product_Menu.dart';
import 'Views/Store/Store_ReportUser/S_ReportUser.dart';
import 'Views/Store/Store_calculate_cost/S_Calculate_Cost.dart';
import 'Views/Store/Store_user_manual/S_User_Manual.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),

      //home: homepages(),
     home: Welcome_page(),
      initialRoute: '/',
      routes: {
        "/notifications": (context) => notifications(),
        "/ProfilePageWidget": (context) => ProfilePageWidget(),
        "/homepages": (context) => homepages(),
        "/S_HomepageWidget": (context) => S_HomepageWidget(),
        "/Product_Menu": (context) => Product_Menu(),
        "/S_Other": (context) => S_Other(),
        "/S_user_manual": (context) => S_user_manual(),
        "/ReportUser": (context) => ReportUser(),
        "/S_Calculate_Cost": (context) => S_Calculate_Cost(),
        "/store_information": (context) => store_information(),
        "/Login_Store": (context) =>  Login_Store(),
        "/Welcome_page": (context) =>  Welcome_page(),
        "/Sign_Store": (context) =>   Sign_Store(),
        "/Customer_login": (context) =>   Customer_login(),
        "/Sign_Customer": (context) =>   Sign_Customer(),
        "/Homepage_Customer": (context) =>   Homepage_Customer(),
        "/Customer_ProfilePageWidget": (context) =>   Homepage_Customer(),




















        // notifications.routeName : (context) => notifications()
      },
      //home: HomePage1(),
    );
  }
}

