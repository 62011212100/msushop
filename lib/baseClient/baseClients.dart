import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;

const String baseUrl = 'https://msushop.comsciproject.net/MSUSHOP_API/index.php';

class BaseClient {
  var client = http.Client();

  //Post
  Future<dynamic> post(String api, dynamic object) async {
    var url = Uri.parse(baseUrl + api);
    var _payload = json.encode(object);
    var _headers = {
      'Content-Type': 'application/json',
    };
    var response = await client.post(url, body: _payload, headers: _headers);
    if (response.statusCode == 201) {
      //print(response.body);
      return response.body;

    } else {
      //throw exception and catch it in UI
    }
  }

 /* Future<List<UserModel>?> getUsers(String api) async {
    try {
      var url = Uri.parse(baseUrl + api);
      var response = await http.get(url);
      if (response.statusCode == 200) {
        List<UserModel> _model = userModelFromJson(response.body);
        return _model;
      }
    } catch (e) {
      log(e.toString());
    }
  }*/


}
