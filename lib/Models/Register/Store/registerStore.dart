import 'dart:convert';

List<RegisterStore> registerStoreFromJson(String str) => List<RegisterStore>.from(json.decode(str).map((x) => RegisterStore.fromJson(x)));

String registerStoreToJson(List<RegisterStore> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class RegisterStore {
  RegisterStore({
    required this.registerStoreEmail,
    required this.registerStorePassword,
    required this.registerStoreNameUser,
  });


  String registerStoreEmail;
  String registerStorePassword;
  String registerStoreNameUser;

  factory RegisterStore.fromJson(Map<String, dynamic> json) => RegisterStore(
    registerStoreEmail: json["register_Store_Email"],
    registerStorePassword: json["register_Store_Password"],
    registerStoreNameUser: json["register_Store_NameUser"],
  );

  Map<String, dynamic> toJson() => {
    "register_Store_Email": registerStoreEmail,
    "register_Store_Password": registerStorePassword,
    "register_Store_NameUser": registerStoreNameUser,
  };
}
