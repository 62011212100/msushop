import 'dart:convert';

List<LoginStore> registerStoreFromJson(String str) => List<LoginStore>.from(json.decode(str).map((x) => LoginStore.fromJson(x)));

String registerStoreToJson(List<LoginStore> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class LoginStore {
  LoginStore({
    required this.registerStoreNameUser,
    required this.registerStorePassword,
  });


  String registerStorePassword;
  String registerStoreNameUser;

  factory LoginStore.fromJson(Map<String, dynamic> json) => LoginStore(
    registerStoreNameUser: json["register_Store_NameUser"],
    registerStorePassword: json["register_Store_Password"],
  );

  Map<String, dynamic> toJson() => {
    "register_Store_NameUser": registerStoreNameUser,
    "register_Store_Password": registerStorePassword,
  };
}
