import 'dart:convert';

StatusLoginStore statusLoginStoreFromJson(String str) => StatusLoginStore.fromJson(json.decode(str));

String statusLoginStoreToJson(StatusLoginStore data) => json.encode(data.toJson());

class StatusLoginStore {
  StatusLoginStore({
    required this.status,
    required this.message,
  });

  String status;
  String message;

  factory StatusLoginStore.fromJson(Map<String, dynamic> json) => StatusLoginStore(
    status: json["status"],
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
  };
}
